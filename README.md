# 학습 프로젝트 : 게시판 만들기 

## 프로젝트에 대해서
이번 프로젝트는 스프링을 이용한 웹 개발 과정 중에서 백엔드 과정을 배우기 위한 목적으로 행해진 학습 프로젝트이다.
학습 프로젝트이지만 향후의 개발 전개에 따라 실제 프로젝트까지 갈 수 있도록 실제 프로젝트 과정에 맞게 개발을 진행하였다.
본 프로젝트에는 클라이언트가 존재하며, 클라이언트의 요구에 따라 개발을 진행해야 한다. 개발 기간은 정해져 있으며 
클라이언트의 요구사항을 모두 만족한 다음에는 개발자의 임의대로 추가 개발을 진행할 수 있다.

## 프로젝트 요구사항
프로젝트 요구사항은 클라이언트의 요구사항과 개발자가 받아들인 요구사항으로 나눌 수 있다. 실제 본 프로젝트에서는 개발자가
받아들인 요구사항 밖에 없지만 실제 프로젝트 상황을 상정해서 클라이언트의 요구사항도 임의로 작성해보았다.

### 클라이언트의 요구사항
개발자는 게시판을 구현해야 한다. Server Side Rendering 방식으로 게시판을 구현해야 하며 DB는 MySQL을 사용한다.
프론트 엔드 구현에 필요한 templete은 클라이언트가 기본으로 제공하며, 필요 시 개발자가 임의로 변경할 수 있다. 
개발 언어는 JAVA를 사용해야 하며 스프링을 이용해서 백엔드를 구축해야 한다.

게시판은 게시글을 포함할 수 있으며 존재하는 게시판은 메인 페이지에서 접근 가능해야 한다.
게시판들은 각각 이름과 설명이 존재해야 하며 CRUD가 가능해야 한다. 게시판에 들어가면 게시글들이 있어야 하며 게시글은
페이지 형식으로 볼 수 있어야 한다. 게시글 역시 CRUD가 가능해야 한다. 게시글은 제목과 내용이 존재해야 한다. 게시판에서
키워드를 입력하면 해당 키워드가 제목에 존재하는 게시글만 확인할 수 있어야 한다. 게시글에는 
댓글을 달 수 있으며 댓글을 달면 게시글에서 볼 수 있어야 한다. 게시글은 작성과 수정, 삭제가 가능해야 한다. 게시글을 삭제하면
게시글에 달린 댓글도 지워져야 하며 게시판을 삭제하면 게시판에 달린 게시글, 댓글 역시 모두 지워져야 한다. 

웹에서 에러가 발생하면 
DB에서 오류가 발생했던 일은 롤백되어야 한다. 작업마다 로그를 남겨서 클라이언트가 확인할 수 있어야 한다. 개발 기간은
2주이며 이를 넘어서면 절대 안 되며 2주 안에 개발을 마치면 추가 개발은 가능하다.

### 개발자가 받아들인 요구사항
1. 백엔드 개발에 스프링을 이용하며, SSR 구현으로 Thymeleaf를 사용한다.
2. 도메인은 3개, Board, Post, Comment를 구현해야하며 디렉토리 구성은 Controller-Service-Repository 계층형으로 구성한다.
3. 도메인이 만족해야 하는 공통 요구사항은 다음과 같다: Lombok을 통한 애너테이션 기반 코드 작성, 
DTO-Entity 이원화 및 mapping(mapstruct 사용)이다.
4. Board와 Post는 일대다 양방향 연관관계를 가지며 Post와 Comment는 일대다 단방향 연관관계를 가진다.
5. Post와 Comment는 jpa auditing을 이용하여 생성 일시와 수정 일시가 기록되어야 한다.
6. Repository는 다음과 같이 구성한다: MySQL db 사용, Board 테이블은 jdbc로 접근하며 나머지 두 테이블은 jpa를 사용한다.
7. Service는 다음과 같은 조건을 만족해야한다: 트랜잭션
8. Controller는 다음과 같이 구성한다: view 객체를 반환해야한다.
9. Post controller는 Post의 pagenation을 구현해야 한다.
10. Post와 Comment는 최신 순으로 정렬할 수 있어야 한다.
11. Post의 제목에 keyword가 들어가는 Post만 조회할 수 있어야 한다.:: 
12. 세 계층에서 일을 진행할 때마다 log가 작성되어야 한다. 구현체는 logback을 사용하며 logback 파일을 임의대로 변경할 수 있다. 
13. Board의 Controller, Comment의 service 계층에 대한 테스트 코드가 존재해야 한다. 
14. 개발 기간은 2주이며 2주 안에 개발이 완료되었으면 추가로 REST API도 개발할 수 있다.

## 개발 과정
프로젝트는 계층형으로 구성되었지만 개발은 도메인 별로 했다. 테스트를 만드는 걸 고려하면 계층형으로 개발하는 것이 옳지만
프론트엔드를 직접 만들지 않은 상태에서 특정 페이지가 실제로 구현되는 것을 보고 싶어서 도메인 별로 개발을 진행하였다. 추후
개발을 할 때는 프로젝트의 크기에 따라 유동적으로 개발 순서를 정할 생각이다. 

Repository는 가장 오류가 나기 쉬운 계층이므로 테스트 코드를
항상 작성하였다. 특히 이번 학습 프로젝트에는 같은 DB임에도 Board는 Jdbc를 이용하고 나머지 도메인은 JPA를 이용하기에 
더 오류가 나기 쉬웠다. 테스트 코드를 만들고 성공함으로써 자신감 있게 다음 개발을 진행할 수 있었다. 

Service 계층은 이번 개발 과정에서 비즈니스 로직이 아주 단순하기 때문에 만드는 데 큰 어려움은 없었다. 하지만 jdbc와 Jpa가
엮여있어서 약간의 어려움은 존재했고, 트랜잭션의 개념을 확립하기 전이라 트랙잭션 롤백 테스트를 만드는 데 시간을 많이 썼다.

Controller 계층은 Repository만큼 시간을 썼던 파트인데, 이는 실력 미숙이 있기도 하거니와 타임리프에 대한 개념이 부족한
상황에서 더듬더듬 배워가면서 개발을 한 까닭도 있다. 그럼에도 제대로 구현을 할 수 있어서 만족스럽다.

8~9일 정도를 사용해서 Board, Post, Comment에 대한 CRUD를 세 계층에 대해 모두 구현할 수 있었다. 그 이후에는 로그 생성,
에러 핸들링, REST API 구현 등에 시간을 사용하였다.

개발을 하면서 여러 branch를 이용해 개발을 하였고, 보통은 feature branch에서 작업을 하다가 특정 단위의 일이 끝나면 
dev branch에 request를 하고 프로젝트 담당 코치님에게 의견을 물었고, 모르는 게 있으면 issue로 등록해서 물어보기도 했다.

## 개발 결과
클라이언트의 요구 조건을 모두 충족하는 게시판을 구현했다. 추가로 잘못된 게시판, 게시글, 댓글을 조회하거나 수정하려는 경우에
프로젝트를 위해 만들어진 예외를 출력하며 웹에서 접근할 때는 에러 페이지로 가게 만들었다. 에러 페이지는 3초 뒤에 직전 페이지로
되돌아가게 만들어졌다. 컨트롤러까지 올라온 예외들은 ControllerAdvice를 통해 예외들에 맞는 에러페이지로 가도록 ExceptionHandler를
사용하였다. 

테스트도 db는 모두 작성하였으며 다른 계층도 필요에 따라서는 따로 여분의 테스트를 만들어서 결과를 확인하였다.
웹에서 접근하지 않는 경우에는 REST API를 통해 데이터에 접근할 수 있도록 만들었다. 

## 개발 후기
이주 간의 개발 시간 동안 클라이언트의 요구를 모두 구현하는 게시판을 만들 수 있었다. 그러나 예상했던 기간인 일주일에 비하면
두배 정도 시간이 더 걸렸다. 내 생각보다 코드를 만드는 데 시간이 더 걸렸고, 테스트를 하면서 생기는 오류는 많았다. 그리고
내가 배웠던 것보다 상정할 수 있는 상황이 많았고, 이를 대처하는 데 시간을 많이 썼다.

클라이언트의 요구 조건도 조금 어려웠다.
두 엔티티 사이에 일대다 단방향은 보통 지양하는 편이라 양방향으로 코드를 작성하였는데 이는 개발 조건을 무시한 것이라 다시
코드를 작성하기도 했다. 또한 동일한 DB를 두 가지 방식으로 접근하다보니 익숙하지 않아서 오류도 많이 발생했다.

또한 내가 모르는 것이 많다는 것을 체감한 것도 큰 수확이다. 프로젝트를 진행하면서 스프링 전반, JPA, HTTP,
그리고 WEB 전반에 대해 모르는 것이 뭔지 조금 알았고 공부 방향을 잡을 수 있었다. 다음 프로젝트를 진행할 때는 이번의 경험을
토대로 더 나은 결과를 얻어 볼 생각이다.

## 개발하면서 어려웠던 점
1. 가장 처음 어려웠던 점은 git을 사용하는 데서 발생했다. 제일 처음에 gitlab에 push할 때 쓸데없는 파일도 올라가는 일이
있었다. 디렉토리나 파일을 지정해서 add를 하는 것도 방법이지만 좀 더 좋은 방법이 없을까 찾아보다가 .gitIgnore 파일을 
다루는 방법을 배웠다. 특히 application.properties와 같은 파일은 보안 문제로 git에 올리지 않는 것이 좋다는 것을 배웠다.
   (하지만 코치님도 내 코드를 돌려보셔야 했기 때문에 결국 업로드는 했다.)
2. Repository에 대한 단위 테스트를 진행할 때 @BeforeEach로 임의 데이터를 넣어주는 경우, @Transactional로 롤백이
깔끔하게 되지 않았다. 특히 각 도메인의 Entity마다 id를 할당했는데, 단위 테스트를 하는 동안 id가 리셋되지 않는 문제가
발생했다. 공부를 하고 나니 MySQL에서 auto_increment와 같은 seqeuence로 주어지는 값들은 rollback을 해도 처음으로
돌아가지 않는 다는 것을 알았다. 그래서 각 테스트가 끝나면 테이블을 지우고 새로 만드는 sql를 db에 날리도록 
@Sql을 이용해서 문제를 해결하였다. 
3. Post를 생성할 때  board_id 값을 view에서 받아와서 board_id에 해당하는 Board를 db에서 받아와서 Post에 넣고, Post
를 db에 저장하는 등 db에 두 번 접속해야 하는 비효율성이 존재한다는 것을 발견했다. 이는 ORM인 JPA를 사용함으로써 생기는
문제로 view에서 처음부터 boardDTO 객체를 받아온다면 db에 접속을 줄일 수 있을 것 같았다. 하지만 BoardDTO가 무거워진다면
웹에서 서버로 데이터를 많이 보내야 하는 문제가 발생하므로 코드를 수정하지 않았다. 이 문제는 추후 다른 개발자의 조언을 듣고
고민을 더 해볼 예정이다.
4. JdbcTemplete를 사용하는  Board 객체를 지울 때 먼저 Jpa를 사용하는 Post 객체를 미리 지웠음에도 오류가 발생했다.
이는 영속성에 대한 개념이 부족했던 것과, 다른 db 접근 방식을 혼합했기에 생긴 문제였다. 그래서 Post를 db에서 모두 지우게
한 다음에 flush()를 사용함으로써 Board를 지울 때 문제가 없도록 만들었다. Post를 db에서 지울 때는 CascadeType.DELETE
를 적용함으로써 Comment가 자동으로 지워지게 만들었다. 실제 db를 운영할 때는 데이터를 지우지 않는 것이 좋으므로 추후에는
코드를 변경할 수도 있다.
5. @DeleteMapping으로 Comment를 지운 후에 특정 uri로 redirect를 하는 코드를 작성했다. 동일한 uri로 get과 delete
method가 존재하였는데, 후자의 메서드로 매칭되어서 Post가 지워지는 일이 발생하였다. 이는 스프링의 redirect가 상태코드
302번을 쓰는 것과 관련이 있었는데, 302번으로 응답하면 redirect시 항상 GetMapping을 하지는 않는다는 것을 배웠다. 그래서
상태코드 303번으로 redirect하도록 변경했을 때 원하는 대로 코드가 작동했다.
6. logback을 이용해서 파일에 로그를 작성할 때 콘솔에도 로그가 출력되는 문제가 발생했다. 이는 root 레벨 로그가 다른 로그
설정에 상속되는 것이 원인으로, logback-spring.xml에서 logger에 additivity='false' 설정을 줌으로서 해결할 수 있었다.
7. view를 반환하는 Controller와 RestController가 동시에 존재할 때 스프링이 ambiguity 에러를 냈다. 이는 Controller
Handler가 어떤 컨트롤러를 정해야 하는 지에 대한 설정이 없어서 생긴 문제로, Controller들에 consume 속성을 정해줌으로써
문제를 해결할 수 있었다. 
수정: GetMapping에 대해서는 이런 식으로 해결할 수 없었다. 왜냐하면 body가 존재하지 않기 때문이다. 그래서 결과적으로
Rest API들은 모두 uri앞에 "/api"를 붙임으로서 문제를 없앨 수 있었다.
8. Board나 Post에 글을 작성할 때 줄바꿈을 해도 저장할 때 줄바꿈이 인식되지 않는 문제가 있다. 이는 아직 해결하지 못한 문제로
추후 공부 후 수정할 계획이다.

## 개발하면서 아쉬웠던 점
처음 개발을 기획할 때는 기본적으로 주어진 html과 다른 디자인의 html을 직접 만들고, 도메인도 세개 이상을 만들 계획을 갖고있었다.
하지만 개발을 진행하다보니 생각보다 시간이 많이 필요했고, 그 결과 기본적으로 주어진 요구사항을 지키고 추가 사항을 몇 개
만드는 것 이상을 하지 못했다. 소설 추천 게시판을 만드는 것이 개발 목표였는데 목표를 달성하지 못한 것 같아 너무 아쉬웠다. 


프로젝트의 원래 기획안은 [ReadMe2](README2.md)에서 볼 수 있다.

## 프로젝트 소감 한 마디
내가 뭘 모르는지를 배울 수 있어서 좋은 프로젝트였다고 생각한다.