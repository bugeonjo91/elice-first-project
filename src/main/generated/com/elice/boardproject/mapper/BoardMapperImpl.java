package com.elice.boardproject.mapper;

import com.elice.boardproject.entity.Board;
import com.elice.boardproject.entity.BoardDTO;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-03-01T18:26:16+0900",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 20.0.2.1 (Amazon.com Inc.)"
)
@Component
public class BoardMapperImpl implements BoardMapper {

    @Override
    public BoardDTO toDTO(Board board) {
        if ( board == null ) {
            return null;
        }

        BoardDTO.BoardDTOBuilder boardDTO = BoardDTO.builder();

        boardDTO.id( board.getId() );
        boardDTO.name( board.getName() );
        boardDTO.description( board.getDescription() );

        return boardDTO.build();
    }

    @Override
    public Board toEntity(BoardDTO boardDTO) {
        if ( boardDTO == null ) {
            return null;
        }

        Board.BoardBuilder board = Board.builder();

        board.id( boardDTO.getId() );
        board.name( boardDTO.getName() );
        board.description( boardDTO.getDescription() );

        return board.build();
    }
}
