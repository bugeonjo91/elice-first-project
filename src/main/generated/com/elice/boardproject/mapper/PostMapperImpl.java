package com.elice.boardproject.mapper;

import com.elice.boardproject.entity.Post;
import com.elice.boardproject.entity.PostDTO;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-03-01T18:26:16+0900",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 20.0.2.1 (Amazon.com Inc.)"
)
@Component
public class PostMapperImpl implements PostMapper {

    @Override
    public PostDTO toDTO(Post post) {
        if ( post == null ) {
            return null;
        }

        PostDTO.PostDTOBuilder postDTO = PostDTO.builder();

        postDTO.id( post.getId() );
        postDTO.title( post.getTitle() );
        postDTO.content( post.getContent() );
        postDTO.createdAt( post.getCreatedAt() );

        return postDTO.build();
    }

    @Override
    public Post toEntity(PostDTO postDTO) {
        if ( postDTO == null ) {
            return null;
        }

        Post.PostBuilder post = Post.builder();

        post.id( postDTO.getId() );
        post.title( postDTO.getTitle() );
        post.content( postDTO.getContent() );

        return post.build();
    }
}
