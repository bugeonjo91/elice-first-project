package com.elice.boardproject.controller.REST;

import com.elice.boardproject.entity.Comment;
import com.elice.boardproject.entity.CommentDTO;
import com.elice.boardproject.service.CommentService;
import com.elice.boardproject.etc.exception.NoSuchPostError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class CommentRestController {



    private final CommentService commentService;
    @Autowired
    public CommentRestController(CommentService commentService) {

        this.commentService = commentService;
    }

    @GetMapping(value = "/api/comments")
    public ResponseEntity getComments(){
        List<CommentDTO> commentsDTO = commentService.findCommentsDTO();
        return new ResponseEntity<>(commentsDTO, HttpStatus.OK);

    }
    @GetMapping(value = "/api/comments/{id}")
    public ResponseEntity getComment(@PathVariable(value="id") Long id){
        CommentDTO commentDTO = commentService.findCommentByIdDTO(id);
        return new ResponseEntity<>(commentDTO, HttpStatus.OK);

    }
    @PostMapping(value = "/api/comments", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity postComment(@RequestParam(value="postId") Long postId, @RequestBody CommentDTO commentDTO){
        CommentDTO createdCommentDTO = commentService.saveCommentDTO(commentDTO, postId);

        return new ResponseEntity<>(createdCommentDTO, HttpStatus.CREATED);

    }

    @PostMapping(value = "/api/comments/{commentId}/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity patchComment(@PathVariable(value="commentId") Long id, @RequestBody CommentDTO commentDTO){
        commentDTO.setId(id);

        commentService.updateCommentDTO(commentDTO);

        return new ResponseEntity<CommentDTO>(HttpStatus.OK);

    }

    @DeleteMapping("/api/comments/{currentComment}")
    public ResponseEntity deleteComment(@PathVariable(value="currentComment") Long id){
        commentService.deleteComment(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }


}
