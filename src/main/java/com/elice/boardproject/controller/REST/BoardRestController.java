package com.elice.boardproject.controller.REST;


import com.elice.boardproject.entity.BoardDTO;
import com.elice.boardproject.entity.CommentDTO;
import com.elice.boardproject.entity.PostDTO;
import com.elice.boardproject.service.BoardService;
import com.elice.boardproject.service.PostService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Slf4j
@RestController
public class BoardRestController {

    private final BoardService boardService;
    private final PostService postService;

    @Autowired
    public BoardRestController(BoardService boardService, PostService postService) {

        this.boardService = boardService;
        this.postService = postService;
    }


    @GetMapping("/api/boards")
    public ResponseEntity getBoards(){
        List<BoardDTO> boardsDTO = boardService.findAllBoardsDTO();

        return new ResponseEntity<>(boardsDTO, HttpStatus.OK);

    }

    @GetMapping("/api/boards/{id}")
    public ResponseEntity getBoard(@RequestParam(name = "keyword", required = false) String keyword, @PathVariable(name = "id") Long id){
        BoardDTO boardDTO = boardService.findBoardByIdDTO(id);
        List<PostDTO> postsDTO = postService.findPostsByBoardIdAndTitleContainingDTO(id, (keyword != null) ? keyword : "");
        boardDTO.setPostsDTO(postsDTO);

        return new ResponseEntity<>(boardDTO, HttpStatus.OK);

    }


    @PostMapping("/api/boards/create")
    public ResponseEntity postBoard(@RequestBody BoardDTO boardDTO){
        BoardDTO createdBoardDTO = boardService.saveBoardDTO(boardDTO);

        return new ResponseEntity<>(createdBoardDTO, HttpStatus.CREATED);

    }

    @PostMapping("/api/boards/{id}/edit")
    public ResponseEntity patchBoard(@PathVariable(name="id") Long id, @RequestBody BoardDTO boardDTO){
        boardDTO.setId(id);

        boardService.updateBoardDTO(boardDTO);

        return new ResponseEntity<>(HttpStatus.OK);

    }

    @DeleteMapping("/api/boards/{id}/delete")
    public ResponseEntity deleteBoardById(@PathVariable(name="id") Long id){
        boardService.deleteBoard(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }



}
