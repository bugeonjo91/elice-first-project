package com.elice.boardproject.controller.REST;


import com.elice.boardproject.entity.CommentDTO;
import com.elice.boardproject.entity.PostDTO;
import com.elice.boardproject.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@RestController
public class PostRestController {



    private final PostService postService;


    @Autowired
    public PostRestController(PostService postService) {

        this.postService = postService;
    }



    @GetMapping("/api/posts/{id}")
    public ResponseEntity getPost(@PathVariable(name = "id") Long id){
        PostDTO postDTO = postService.findPostByIdDTO(id);
        return new ResponseEntity<>(postDTO, HttpStatus.OK);

    }

    @PostMapping("/api/posts/create")
    public ResponseEntity postPost(@RequestParam(value="boardId") Long id, @RequestBody PostDTO postDTO){
        PostDTO createdPostDTO = postService.savePostDTO(postDTO, id);

        return new ResponseEntity<>(createdPostDTO, HttpStatus.OK);

    }





    @PostMapping("/api/posts/{id}/edit")
    public ResponseEntity patchPost(@PathVariable(name="id") Long id, @RequestBody PostDTO postDTO){
        PostDTO createdPostDTO = postService.updatePostDTO(postDTO, id);

        return new ResponseEntity<>(createdPostDTO, HttpStatus.OK);


    }

    @ResponseStatus(HttpStatus.SEE_OTHER)
    @DeleteMapping("/api/posts/{id}")
    public ResponseEntity deletePost(@PathVariable(name="id") Long id){
        postService.deletePostById(id);
        return new ResponseEntity<>( HttpStatus.NO_CONTENT);
    }



}