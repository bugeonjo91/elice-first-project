package com.elice.boardproject.controller.web;


import com.elice.boardproject.entity.CommentDTO;
import com.elice.boardproject.entity.PostDTO;
import com.elice.boardproject.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class PostController {



    private final PostService postService;


    @Autowired
    public PostController(PostService postService) {

        this.postService = postService;
    }



    @GetMapping("/posts/{id}")
    public String getPost(@PathVariable(name = "id") Long id, Model model){
        PostDTO postDTO = postService.findPostByIdDTO(id);
        List<CommentDTO> commentsDTO = postDTO.getCommentsDTO();

        model.addAttribute("post", postDTO);
        model.addAttribute("comments", commentsDTO);


        return "/post/post";

    }

    @GetMapping("/posts/{id}/edit")
    public String getEditPost(@PathVariable(name = "id") Long id, Model model){
        PostDTO postDTO = postService.findPostByIdDTO(id);
        model.addAttribute("post", postDTO);
        return "/post/editPost";

    }

    @GetMapping("/posts/create")
    public String getCreatePost(@RequestParam(value="boardId") Long id, Model model){
        model.addAttribute("boardId", id);
        return "/post/createPost";

    }

    @PostMapping("/posts/create")
    public String postPost(@RequestParam(value="boardId") Long id, @RequestParam(value="title") String title, @RequestParam(value="content") String content, RedirectAttributes redirectAttributes){
        PostDTO postDTO = PostDTO.builder().title(title).content(content).build();
        postService.savePostDTO(postDTO, id);
        redirectAttributes.addAttribute("boardId", id);

        return "redirect:/boards/{boardId}";

    }





    @PostMapping("/posts/{id}/edit")
    public String patchPost(@PathVariable(name="id") Long id, @RequestParam(value="title") String title, @RequestParam(value="content") String content){
        PostDTO postDTO = PostDTO.builder().id(id).title(title).content(content).build();
        postService.updatePostDTO(postDTO, id);

        return "redirect:/posts/{id}";

    }

    @ResponseStatus(HttpStatus.SEE_OTHER)
    @DeleteMapping("/posts/{id}")
    public String deletePost(@RequestParam(value="boardId") Long boardId, @PathVariable(name="id") Long id, RedirectAttributes redirectAttributes){
        postService.deletePostById(id);
        redirectAttributes.addAttribute("boardId", boardId);
        return "redirect:/boards/{boardId}";
    }



}