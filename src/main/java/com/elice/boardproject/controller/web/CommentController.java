package com.elice.boardproject.controller.web;

import com.elice.boardproject.entity.CommentDTO;
import com.elice.boardproject.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
public class CommentController {



    private final CommentService commentService;
    @Autowired
    public CommentController(CommentService commentService) {

        this.commentService = commentService;
    }


    @PostMapping(value = "/comments")
    public String postComment(@RequestParam(value="postId") Long postId, @RequestParam(value="content") String content, RedirectAttributes redirectAttributes){
        CommentDTO commentDTO = CommentDTO.builder().content(content).build();
        commentService.saveCommentDTO(commentDTO, postId);
        redirectAttributes.addAttribute("postId", postId);

        return "redirect:/posts/{postId}";

    }

    @PostMapping(value = "/comments/{commentId}/edit")
    public String patchComment(@PathVariable(value="commentId") Long id, @RequestParam(value="content") String content, @RequestParam(value="postId") Long postId, RedirectAttributes redirectAttributes){
        CommentDTO commentDTO = CommentDTO.builder().id(id).content(content).build();
        commentService.updateCommentDTO(commentDTO);
        redirectAttributes.addAttribute("postId", postId);

        return "redirect:/posts/{postId}";

    }



    @ResponseStatus(HttpStatus.SEE_OTHER)
    @DeleteMapping(value = "/comments/{currentComment}")
    public String deleteComment(@PathVariable(value="currentComment") Long id, @RequestParam(value="postId") Long postId, RedirectAttributes redirectAttributes){
        commentService.deleteComment(id);
        redirectAttributes.addAttribute("postId", postId);
        return "redirect:/posts/{postId}";

    }



}
