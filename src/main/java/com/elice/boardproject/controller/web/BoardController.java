package com.elice.boardproject.controller.web;


import com.elice.boardproject.entity.BoardDTO;
import com.elice.boardproject.service.BoardService;
import com.elice.boardproject.entity.PostDTO;
import com.elice.boardproject.service.PostService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Slf4j
@Controller
public class BoardController {

    private final BoardService boardService;
    private final PostService postService;

    @Autowired
    public BoardController(BoardService boardService, PostService postService) {

        this.boardService = boardService;
        this.postService = postService;
    }


    @GetMapping("/boards")
    public String getBoards(Model model){
        List<BoardDTO> boardsDTO = boardService.findAllBoardsDTO();

        model.addAttribute("boards", boardsDTO);
        return "/board/boards";

    }

    @GetMapping("/boards/{id}")
    public String getBoard(@RequestParam(name = "page", defaultValue = "0") int page, @RequestParam(name = "size",  defaultValue = "10") int size, @RequestParam(name = "keyword", required = false) String keyword, @PathVariable(name = "id") Long id, Model model){
        BoardDTO boardDTO = boardService.findBoardByIdDTO(id);
        model.addAttribute("board", boardDTO);

        Page<PostDTO> postsDTO = postService.findPostsByBoardIdAndTitleContainingDTO(id,(keyword != null) ? keyword : "", page, size);
        model.addAttribute("postPage", postsDTO);
        model.addAttribute("keyword", keyword);

        return "/board/board";

    }

    @GetMapping("/boards/{id}/edit")
    public String geteditBoard(@PathVariable(name = "id") Long id, Model model){
        BoardDTO boardDTO = boardService.findBoardByIdDTO(id);
        model.addAttribute("board", boardDTO);
        return "/board/editBoard";

    }


    @GetMapping("/boards/create")
    public String getCreateBoard(Model model){
        return "/board/createBoard";

    }

    @PostMapping("/boards/create")
    public String postBoard(@RequestParam(value="name") String name, @RequestParam(value="description") String description){
        BoardDTO boardDTO = BoardDTO.builder().name(name).description(description).build();
        boardService.saveBoardDTO(boardDTO);

        return "redirect:/boards";

    }

    @PostMapping("/boards/{id}/edit")
    public String patchBoard(@PathVariable(name="id") Long id, @RequestParam(value="name") String name, @RequestParam(value="description") String description){
        BoardDTO boardDTO = BoardDTO.builder().id(id).name(name).description(description).build();
        boardService.updateBoardDTO(boardDTO);

        return "redirect:/boards";

    }
    @ResponseStatus(HttpStatus.SEE_OTHER)
    @DeleteMapping("/boards/{id}/delete")
    public String deleteBoardById(@PathVariable(name="id") Long id){
        boardService.deleteBoard(id);

        return "redirect:/boards";

    }



}
