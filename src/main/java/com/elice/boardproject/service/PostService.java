package com.elice.boardproject.service;


import com.elice.boardproject.entity.Board;
import com.elice.boardproject.mapper.BoardMapper;
import com.elice.boardproject.repository.BoardRepository;
import com.elice.boardproject.entity.Comment;
import com.elice.boardproject.mapper.CommentMapper;
import com.elice.boardproject.service.CommentService;
import com.elice.boardproject.etc.exception.NoSuchPostError;
import com.elice.boardproject.entity.Post;
import com.elice.boardproject.entity.PostDTO;
import com.elice.boardproject.mapper.PostMapper;
import com.elice.boardproject.repository.PostRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class PostService {

    private final BoardRepository boardRepository;
    private final PostRepository postRepository;
    private final CommentService commentService;
    private final BoardMapper boardMapper;
    private final PostMapper postMapper;
    private final CommentMapper commentMapper;


    @Autowired
    public PostService(BoardRepository boardRepository, PostRepository postRepository, CommentService commentService, BoardMapper boardMapper, PostMapper postMapper, CommentMapper commentMapper) {
        this.boardRepository = boardRepository;
        this.postRepository = postRepository;
        this.commentService = commentService;
        this.postMapper = postMapper;
        this.commentMapper = commentMapper;
        this.boardMapper = boardMapper;
    }


    public PostDTO findPostByIdDTO(Long id) {

        try{
            Post post = postRepository.getReferenceById(id);
            List<Comment> comments = post.getComments();

            PostDTO postDTO = postMapper.toDTO(post);
            postDTO.setBoardDTO(boardMapper.toDTO(post.getBoard()));

            postDTO.setCommentsDTO(comments.stream().map(commentMapper::toDTO).collect(Collectors.toList()));
            return postDTO;

        } catch (EntityNotFoundException e){
            throw new NoSuchPostError(e.getMessage(), e);
        }

    }
//
//    public Post findPostById(Long id) {
//        return postRepository.getReferenceById(id);
//    }
//
//    public List<PostDTO> findPostsByBoardIdDTO(Long id) {
//
//        return postRepository.findByBoard_Id(id).stream().map(postMapper::toDTO).collect(Collectors.toList());
//
//    }
//
//    public List<Post> findPostsByBoardId(Long id) {
//
//        return postRepository.findByBoard_Id(id);
//
//    }
//
//    public Page<PostDTO> findPostsByBoardIdDTO(Long id, int page, int size) {
//        return postRepository.findByBoard_Id(id, PageRequest.of(page, size, Sort.by("id").descending())).map(postMapper::toDTO);
//    }

    public Page<PostDTO> findPostsByBoardIdAndTitleContainingDTO(Long id,String keyword, int page, int size) {
        return postRepository.findByBoard_IdAndTitleContaining(id,keyword, PageRequest.of(page, size, Sort.by("id").descending())).map(postMapper::toDTO);
    }
    public List<PostDTO> findPostsByBoardIdAndTitleContainingDTO(Long id,String keyword) {
        return postRepository.findByBoard_IdAndTitleContaining(id,keyword).stream().map(postMapper::toDTO).collect(Collectors.toList());
    }
//
//    public Page<Post> findPostsByBoardId(Long id, int page, int size) {
//        return postRepository.findByBoard_Id(id, PageRequest.of(page, size, Sort.by("id").descending()));
//    }
//    public Page<Post> findPostsByBoardIdAndTitleContaining(Long id,String keyword, int page, int size) {
//        return postRepository.findByBoard_IdAndTitleContaining(id,keyword, PageRequest.of(page, size, Sort.by("id").descending()));
//    }

    public PostDTO savePostDTO(PostDTO postDTO, Long boardId){
        //DTO에서 받아오는 id는 무의미하다.
        postDTO.setId(null);
        Board board = boardRepository.findById(boardId);
        Post post = postMapper.toEntity(postDTO);
        post.setBoard(board);

        return postMapper.toDTO(postRepository.save(post));

    }
//
//    public Post savePost(Post post){
//
//        return postRepository.save(post);
//
//    }

    public PostDTO updatePostDTO(PostDTO postDTO, Long postId){

        try{
            Post postToBeUpdated = postRepository.getReferenceById(postId);
            postToBeUpdated.setTitle(postDTO.getTitle());
            postToBeUpdated.setContent(postDTO.getContent());
            return postMapper.toDTO(postRepository.save(postToBeUpdated));

        } catch (EntityNotFoundException e){
            throw new NoSuchPostError(e.getMessage(), e);
        }



    }

//
//    public Post updatePost(PostDTO post){
//        Post postToBeUpdated = postRepository.getReferenceById(post.getId());
//        postToBeUpdated.setTitle(post.getTitle());
//        postToBeUpdated.setContent(post.getContent());
//        return postRepository.save(postToBeUpdated);
//
//    }

    public void deletePostsByBoardId(Long id){
        postRepository.deleteByBoard_Id(id);
        postRepository.flush();

    }

    public void deletePostById(Long id) {
        postRepository.deleteById(id);
    }
}
