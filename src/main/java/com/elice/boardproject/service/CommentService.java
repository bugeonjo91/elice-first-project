package com.elice.boardproject.service;


import com.elice.boardproject.entity.Comment;
import com.elice.boardproject.entity.CommentDTO;
import com.elice.boardproject.mapper.CommentMapper;
import com.elice.boardproject.repository.CommentRepository;
import com.elice.boardproject.etc.exception.NoSuchCommentError;
import com.elice.boardproject.etc.exception.NoSuchPostError;
import com.elice.boardproject.entity.Post;
import com.elice.boardproject.repository.PostRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Service
public class CommentService {


    private final PostRepository postRepository;
    private final CommentRepository commentRepository;
    private final CommentMapper commentMapper;


    @Autowired
    public CommentService(PostRepository postRepository, CommentRepository commentRepository, CommentMapper commentMapper) {
        this.postRepository = postRepository;
        this.commentRepository = commentRepository;
        this.commentMapper = commentMapper;
    }

//    public Comment findCommentById(Long id) {
//        return commentRepository.getReferenceById(id);
//    }
    public CommentDTO findCommentByIdDTO(Long id) {

        try{
            return commentMapper.toDTO(commentRepository.getReferenceById(id));
        } catch (EntityNotFoundException e){
            throw new NoSuchCommentError(e.getMessage(), e);
        }

    }
    public List<Comment> findComments() {
        return commentRepository.findAll();
    }
    public List<CommentDTO> findCommentsDTO() {
        return commentRepository.findAll().stream().map(commentMapper::toDTO).collect(Collectors.toList());
    }


    public CommentDTO saveCommentDTO(CommentDTO commentDTO, Long postId) {
        //DTO에서 받아오는 id는 무의미하다.
        commentDTO.setId(null);


        try{
            Post post = postRepository.getReferenceById(postId);
            Comment createdComment = commentRepository.save(commentMapper.toEntity(commentDTO));
            post.getComments().add(createdComment);
            postRepository.save(post);
            return commentMapper.toDTO(createdComment);

        } catch (EntityNotFoundException e){
            throw new NoSuchPostError(e.getMessage(), e);
        }


    }
//    public Comment saveComment(Comment comment) {
//        return commentRepository.save(comment);
//    }

    public CommentDTO updateCommentDTO(CommentDTO commentDTO) {



        try{
            Comment commentToBeUpdated = commentRepository.getReferenceById(commentDTO.getId());
            commentToBeUpdated.setContent(commentDTO.getContent());
            return commentMapper.toDTO(commentRepository.save(commentToBeUpdated));

        } catch (EntityNotFoundException e){
            throw new NoSuchCommentError(e.getMessage(), e);
        }

    }

//    public Comment updateComment(Comment comment) {
//
//        Comment commentToBeUpdated = commentRepository.getReferenceById(comment.getId());
//        commentToBeUpdated.setContent(comment.getContent());
//        return commentRepository.save(commentToBeUpdated);
//    }

    public void deleteComment(Long id) {
        commentRepository.deleteById(id);
    }





}

