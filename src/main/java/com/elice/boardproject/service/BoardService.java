package com.elice.boardproject.service;


import com.elice.boardproject.entity.Board;
import com.elice.boardproject.entity.BoardDTO;
import com.elice.boardproject.mapper.BoardMapper;
import com.elice.boardproject.repository.BoardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class BoardService {
    private final BoardRepository boardRepository;
    private final PostService postService;

    private final BoardMapper boardMapper;

    @Autowired
    public BoardService(BoardRepository boardRepository, PostService postService, BoardMapper boardMapper) {
        this.boardRepository = boardRepository;
        this.postService = postService;
        this.boardMapper = boardMapper;
    }

    public List<Board> findAllBoards(){

        return boardRepository.findAll();

    }

    public List<BoardDTO> findAllBoardsDTO(){

        return boardRepository.findAll().stream().map(boardMapper::toDTO).collect(Collectors.toList());

    }
    public Board findBoardById(Long id){

        return boardRepository.findById(id);
    }

    public BoardDTO findBoardByIdDTO(Long id){

        return boardMapper.toDTO(boardRepository.findById(id));
    }

    public BoardDTO saveBoardDTO(BoardDTO boardDTO){
        //DTO에서 받아오는 id는 무의미하다.
        boardDTO.setId(null);

        return boardMapper.toDTO(boardRepository.save(boardMapper.toEntity(boardDTO)));
    }

    public Board saveBoard(Board board){
        return boardRepository.save(board);
    }

    public int updateBoardDTO(BoardDTO boardDTO){
        return boardRepository.update(boardMapper.toEntity(boardDTO));
    }

    public int updateBoard(Board board){
        return boardRepository.update(board);
    }

    public void deleteBoard(Long id){

        postService.deletePostsByBoardId(id);
        boardRepository.delete(id);
    }


}
