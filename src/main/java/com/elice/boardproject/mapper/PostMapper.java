package com.elice.boardproject.mapper;



import com.elice.boardproject.entity.Post;
import com.elice.boardproject.entity.PostDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PostMapper {


    public PostDTO toDTO(Post post);
    public Post toEntity(PostDTO postDTO);

}
