package com.elice.boardproject.mapper;


import com.elice.boardproject.entity.Board;
import com.elice.boardproject.entity.BoardDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface BoardMapper {

    public BoardDTO toDTO(Board board);
    public Board toEntity(BoardDTO boardDTO);
}
