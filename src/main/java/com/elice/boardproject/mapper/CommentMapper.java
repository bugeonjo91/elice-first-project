package com.elice.boardproject.mapper;



import com.elice.boardproject.entity.Comment;
import com.elice.boardproject.entity.CommentDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CommentMapper {

    public CommentDTO toDTO(Comment comment);
    public Comment toEntity(CommentDTO commentDTO);
}
