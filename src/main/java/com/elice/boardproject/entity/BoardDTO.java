package com.elice.boardproject.entity;


import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BoardDTO {

    private Long id;
    private String name;
    private String description;
    @Builder.Default
    private List<PostDTO> PostsDTO = new ArrayList<>();

}
