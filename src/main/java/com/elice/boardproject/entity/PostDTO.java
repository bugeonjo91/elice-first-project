package com.elice.boardproject.entity;


import com.elice.boardproject.entity.Board;
import com.elice.boardproject.entity.CommentDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PostDTO {

    private Long id;
    private String title;
    private String content;
    private LocalDateTime createdAt;
    private BoardDTO boardDTO;

    @Builder.Default
    private List<CommentDTO> commentsDTO = new ArrayList<CommentDTO>();
}
