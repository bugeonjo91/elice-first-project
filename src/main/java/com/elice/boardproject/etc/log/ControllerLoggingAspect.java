package com.elice.boardproject.etc.log;


import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Slf4j
@Aspect
@Component
public class ControllerLoggingAspect {

    @Pointcut("execution(* com.elice.boardproject.controller.web.*.*(..))")
    private void controllerTargetMethod(){};
    @Around("controllerTargetMethod()")
    private String aroundLog(ProceedingJoinPoint joinPoint) throws Throwable {

        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
//        log.info(String.format("URI: %-20s HTTP Method: %-7s Controller: %-20s method: %-15s 요청", httpServletRequest.getRequestURI() + ",", httpServletRequest.getMethod()+",", joinPoint.getTarget().getClass().getSimpleName() + ",", joinPoint.getSignature().getName() + ","));
        log.info(
                "URI: {} HTTP Method: {} Controller: {} method: {} 요청",
                String.format("%-20s",httpServletRequest.getRequestURI()),
                String.format("%-7s",httpServletRequest.getMethod()),
                String.format("%-20s",joinPoint.getTarget().getClass().getSimpleName()),
                String.format("%-15s",joinPoint.getSignature().getName())
        );
        String result = (String) joinPoint.proceed();

        if(result.contains("redirect")){
            log.info(
                    "URI: {} HTTP Method: {} Controller: {} method: {} 요청 완료, redirect: {}",
                    String.format("%-20s",httpServletRequest.getRequestURI()),
                    String.format("%-7s",httpServletRequest.getMethod()),
                    String.format("%-25s",joinPoint.getTarget().getClass().getSimpleName()),
                    String.format("%-15s",joinPoint.getSignature().getName()),
                    result.replaceAll("redirect:", "")
            );
            return result;
        }
        log.info(
                "URI: {} HTTP Method: {} Controller: {} method: {} 요청 완료",
                String.format("%-20s",httpServletRequest.getRequestURI()),
                String.format("%-7s",httpServletRequest.getMethod()),
                String.format("%-25s",joinPoint.getTarget().getClass().getSimpleName()),
                String.format("%-15s",joinPoint.getSignature().getName())
        );
        return result;

    }


    @Pointcut("execution(* com.elice.boardproject.controller.REST.*.*(..))")
    private void RestControllerTargetMethod(){};
    @Before("RestControllerTargetMethod()")
    private void beforeLog(JoinPoint joinPoint) {

        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
//        log.info(String.format("URI: %-20s HTTP Method: %-7s Controller: %-20s method: %-15s 요청", httpServletRequest.getRequestURI() + ",", httpServletRequest.getMethod()+",", joinPoint.getTarget().getClass().getSimpleName() + ",", joinPoint.getSignature().getName() + ","));
        log.info(
                "URI: {} HTTP Method: {} Controller: {} method: {} 요청",
                String.format("%-20s", httpServletRequest.getRequestURI()),
                String.format("%-7s", httpServletRequest.getMethod()),
                String.format("%-25s", joinPoint.getTarget().getClass().getSimpleName()),
                String.format("%-15s", joinPoint.getSignature().getName())
        );
    }

    @After("RestControllerTargetMethod()")
    private void afterLog(JoinPoint joinPoint) {

        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        log.info(
                "URI: {} HTTP Method: {} Controller: {} method: {} 요청 완료",
                String.format("%-20s", httpServletRequest.getRequestURI()),
                String.format("%-7s", httpServletRequest.getMethod()),
                String.format("%-25s", joinPoint.getTarget().getClass().getSimpleName()),
                String.format("%-15s", joinPoint.getSignature().getName())
        );


    }

}
