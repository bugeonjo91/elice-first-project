package com.elice.boardproject.etc.log;


import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Slf4j
@Aspect
@Component
public class ServiceLoggingAspect {

    @Pointcut("execution(* com.elice.boardproject..service.*.*(..))")
    private void serviceTargetMethod(){};

    @Before("serviceTargetMethod()")
    private void beforeLog(JoinPoint joinPoint) throws Throwable {
        log.debug(
                "Service: {} method: {} 요청",
                String.format("%-33s",joinPoint.getTarget().getClass().getSimpleName()),
                String.format("%-40s",joinPoint.getSignature().getName())
        );
    }


    @After("serviceTargetMethod()")
    private void afterLog(JoinPoint joinPoint) throws Throwable {

        log.debug(
                "Service: {} method: {} 요청 완료",
                String.format("%-33s",joinPoint.getTarget().getClass().getSimpleName()),
                String.format("%-40s",joinPoint.getSignature().getName())
        );
    }


}
