package com.elice.boardproject.etc.log;


import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.framework.AopProxyUtils;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
public class RepositoryLoggingAspect {

    @Pointcut("execution(* com.elice.boardproject..repository.*.*(..))")
    private void repositoryTargetMethod(){};

    @Before("repositoryTargetMethod()")
    private void beforeLog(JoinPoint joinPoint) throws Throwable {
        String targetClassName = joinPoint.getTarget().getClass().getSimpleName();
        if(joinPoint.getTarget() instanceof Advised) {
            targetClassName = ((Advised) joinPoint.getTarget()).getProxiedInterfaces()[0].getSimpleName();
        }

        log.trace(
                "Repository: {} method: {} 요청",
                String.format("%-30s",targetClassName),
                String.format("%-40s",joinPoint.getSignature().getName())
        );

    }


    @After("repositoryTargetMethod()")
    private void afterLog(JoinPoint joinPoint) throws Throwable {

        String targetClassName = joinPoint.getTarget().getClass().getSimpleName();
        if(joinPoint.getTarget() instanceof Advised) {
            targetClassName = ((Advised) joinPoint.getTarget()).getProxiedInterfaces()[0].getSimpleName();
        }

        log.trace(
                "Repository: {} method: {} 요청 완료",
                String.format("%-30s",targetClassName),
                String.format("%-40s",joinPoint.getSignature().getName())
        );

    }

}
