package com.elice.boardproject.etc.exception;


import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/error")
public class BasicErrorController implements ErrorController {

    private static final Logger consoleLogger = LoggerFactory.getLogger("com.elice.boardproject.etc.exception");

    @GetMapping
    public String handleError(HttpServletRequest request) {
        // 에러 처리 로직
        consoleLogger.error("선택한 페이지는 존재하지 않습니다.");
        // 에러 페이지로 포워딩
        return "error";
    }

    @GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity handleErrorRest(HttpServletRequest request) {
        // 에러 처리 로직
        consoleLogger.error("선택한 페이지는 존재하지 않습니다.");
        // 에러 페이지로 포워딩
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

}


