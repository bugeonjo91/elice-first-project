package com.elice.boardproject.etc.exception;


import lombok.Getter;

@Getter
public class NoSuchBoardError extends RuntimeException{
    public NoSuchBoardError(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }

    public NoSuchBoardError(String errorMessage) {
        super(errorMessage);
    }


}
