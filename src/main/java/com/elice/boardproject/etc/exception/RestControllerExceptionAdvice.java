package com.elice.boardproject.etc.exception;


import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice(basePackages = "com.elice.boardproject.controller.REST")
@Slf4j
public class RestControllerExceptionAdvice {

    private static final Logger consoleLogger = LoggerFactory.getLogger("com.elice.boardproject.etc.exception");
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoSuchBoardError.class)
    public ResponseEntity noSuchBoardError(NoSuchBoardError ex){

        consoleLogger.error("선택한 게시판은 존재하지 않습니다.");
        log.error("선택한 게시판은 존재하지 않습니다.", ex);
        return new ResponseEntity<>("선택한 게시판은 존재하지 않습니다.", HttpStatus.NOT_FOUND);
    }



    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoSuchPostError.class)
    public ResponseEntity noSuchPostError(NoSuchPostError ex){
        consoleLogger.error("선택한 게시글은 존재하지 않습니다.");
        log.error("선택한 게시글은 존재하지 않습니다.", ex);
        return new ResponseEntity<>("선택한 게시글은 존재하지 않습니다.", HttpStatus.NOT_FOUND);
    }


    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoSuchCommentError.class)
    public ResponseEntity noSuchCommentError(NoSuchCommentError ex){
        consoleLogger.error("선택한 답글은 존재하지 않습니다.");
        log.error("선택한 답글은 존재하지 않습니다.", ex);

        return new ResponseEntity<>("선택한 답글은 존재하지 않습니다.", HttpStatus.NOT_FOUND);
    }
}
