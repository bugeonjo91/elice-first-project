package com.elice.boardproject.etc.exception;


import lombok.Getter;

@Getter
public class NoSuchPostError extends RuntimeException{
    public NoSuchPostError(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }

    public NoSuchPostError(String errorMessage) {
        super(errorMessage);
    }


}
