package com.elice.boardproject.etc.exception;


import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice(basePackages = "com.elice.boardproject.controller.web")
@Slf4j
public class WebControllerExceptionAdvice {

    private static final Logger consoleLogger = LoggerFactory.getLogger("com.elice.boardproject.etc.exception");
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoSuchBoardError.class)
    public String noSuchBoardError(NoSuchBoardError ex){

        consoleLogger.error("선택한 게시판은 존재하지 않습니다.");
        log.error("선택한 게시판은 존재하지 않습니다.", ex);
        return "error/noSuchBoardError";
    }



    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoSuchPostError.class)
    public String noSuchPostError(NoSuchPostError ex){
        consoleLogger.error("선택한 게시글은 존재하지 않습니다.");
        log.error("선택한 게시글은 존재하지 않습니다.", ex);
        return "error/noSuchPostError";
    }


    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoSuchCommentError.class)
    public String noSuchCommentError(NoSuchCommentError ex){
        consoleLogger.error("선택한 답글은 존재하지 않습니다.");
        log.error("선택한 답글은 존재하지 않습니다.", ex);

        return "error/noSuchCommentError";
    }
}
