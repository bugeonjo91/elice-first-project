package com.elice.boardproject.etc.exception;


import lombok.Getter;

@Getter
public class NoSuchCommentError extends RuntimeException{
    public NoSuchCommentError(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }

    public NoSuchCommentError(String errorMessage) {
        super(errorMessage);
    }


}
