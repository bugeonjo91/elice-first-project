package com.elice.boardproject;

import com.elice.boardproject.entity.Board;
import com.elice.boardproject.repository.BoardRepository;
import com.elice.boardproject.entity.Comment;
import com.elice.boardproject.repository.CommentRepository;
import com.elice.boardproject.entity.Post;
import com.elice.boardproject.repository.PostRepository;

public class DataInit {
    public DataInit(BoardRepository boardRepository, PostRepository postRepository, CommentRepository commentRepository) {
        Board board = Board.builder()
                .name("SF")
                .description("SF 세계로 여행하는 곳, 미지의 우주 이야기를 나누는 SF 소설 게시판입니다.")
                .build();
        Board board2 = Board.builder()
                .name("미스터리")
                .description("비밀의 매력에 빠져든다! 해결이 기다리는 미스터리 소설들의 세계, 여기에서 만나보세요.")
                .build();
        Board board3 = Board.builder()
                .name("환상 소설")
                .description("현실을 뛰어넘는 환상적인 이야기들이 펼쳐지는 곳, 환상 소설의 마법 같은 세계에 오신 걸 환영합니다.")
                .build();
        boardRepository.save(board);
        boardRepository.save(board2);
        boardRepository.save(board3);

        Board createdBoard = boardRepository.findById(1L);
        Board createdBoard2 = boardRepository.findById(2L);

        Post post = Post.builder()
                .title("살아남기 위한 최후의 게임, '헝거 게임' 소설 추천")
                .content(
                        """
                                '헝거 게임'은 자원 부족으로 빈곤과 굶주림이 난무하는 미래 사회에서 개최되는 생존 게임을 다루는 소설입니다.\n
                                
                                치열한 전투와 정치적 음모가 얽힌 중독적인 이 소설은 독자들을 끊임없는 긴장 속으로 빠져들게 합니다.\n
                                
                                여기서는 캐릭터의 강인한 의지와 복잡한 사회적 메시지가 서로 어우러져 독자들에게 깊은 여운을 남깁니다.
                                """
                ).board(createdBoard)
                .build();
        Post post2 = Post.builder()
                .title("아서 C. 클라크의 환상과 과학의 만남, '유년기의 끝' 소설 추천")
                .content(
                        """
                                아서 C. 클라크의 뛰어난 과학 소설 작품 중 하나인 '유년기의 끝'은 놀라운 상상력과 첨단 기술, 
                                그리고 인류의 진보에 대한 깊은 사유를 담아냅니다. \n
                                클라크의 특유의 풍부한 서사와 미래 예측력이 어우러져 독자들을 현실과 상상의 경계를 넘나들게 만드는 작품입니다.
                                """
                )
                .board(createdBoard2)
                .build();


        Comment comment = Comment.builder()
                .content("이거 진짜 재미있을 것 같아요! 생존 게임 소재에 정치적 음모까지, 완전 내 스타일이에요. 읽는 동안 계속 긴장할 것 같아서 너무 기대돼요!")
                .build();
        Comment comment2 = Comment.builder()
                .content("이런 생존 게임 소재는 좀 지겹지 않아요? 이미 많이 나왔던 소재라서 다소 실망스러워요. 좀 더 새로운 이야기를 찾아볼까 싶네요.")
                .build();
        commentRepository.save(comment);
        commentRepository.save(comment2);

        post.getComments().add(comment);
        post.getComments().add(comment2);
        postRepository.save(post);
        postRepository.save(post2);







    }
}
