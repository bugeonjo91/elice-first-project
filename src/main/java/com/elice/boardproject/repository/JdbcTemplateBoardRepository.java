package com.elice.boardproject.repository;

import com.elice.boardproject.entity.Board;
import com.elice.boardproject.etc.exception.NoSuchBoardError;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.util.List;
import javax.sql.DataSource;


@Repository
public class JdbcTemplateBoardRepository implements BoardRepository {

    private final JdbcTemplate jdbcTemplate;
    private final SimpleJdbcInsert simpleJdbcInsert;

    private final PostRepository postRepository;

    public JdbcTemplateBoardRepository(DataSource dataSource, PostRepository postRepository) {

        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("boards").usingGeneratedKeyColumns("id");
        this.postRepository = postRepository;
    }

    @Override
    public Page<Board> findAll(int page, int size) {
        List<Board> boards = jdbcTemplate.query("SELECT * FROM boards LIMIT ? OFFSET ?", new BeanPropertyRowMapper<Board>(Board.class),page, size);
        return new PageImpl<>(boards);
    }

    @Override
    public List<Board> findAll() {
        List<Board> boards = jdbcTemplate.query("SELECT * FROM boards", new BeanPropertyRowMapper<Board>(Board.class));
        return boards;
    }

    public Board findById(Long id) {
        String sql = "SELECT * FROM boards WHERE id = ?";
        try{
            return jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<Board>(Board.class), id);

        } catch(DataAccessException e){
            throw new NoSuchBoardError(e.getMessage(), e);
        }
    }




    @Override
    public Board save(Board board) {
        board.setId(simpleJdbcInsert.executeAndReturnKey(new BeanPropertySqlParameterSource(board)).longValue());
        return board;
    }

    @Override
    public int update(Board board){
        String sql = "UPDATE boards SET name = ?, description = ? WHERE id = ?";

        int count = jdbcTemplate.update(sql,board.getName(), board.getDescription(), board.getId());

        if(count == 0){
            throw new NoSuchBoardError("There is no such a board");
        }

        return count;
    }

    @Override
    public void delete(Long id) {
        String sql = "DELETE FROM boards WHERE id = ?";
        int count = jdbcTemplate.update(sql,id);
        if(count == 0){
            throw new NoSuchBoardError("There is no such a board");
        }
    }

    @Override
    public void deleteAll() {
        String sql = "DELETE FROM boards";
        postRepository.deleteAll();
        postRepository.findAll();
        jdbcTemplate.update(sql);
    }
}
