package com.elice.boardproject.repository;


import com.elice.boardproject.entity.Board;
import org.springframework.data.domain.Page;

import java.util.List;

public interface BoardRepository  {
    Page<Board> findAll(int page, int size);
    List<Board> findAll();
    Board findById(Long id);
    Board save(Board board);
    int update(Board board);
    void delete(Long id);
    void deleteAll();

}
