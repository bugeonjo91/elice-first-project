package com.elice.boardproject.repository;


import com.elice.boardproject.entity.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post,Long> {
//    Page<Post> findByBoard_Id(Long id, PageRequest pageRequest);
//    List<Post> findByBoard_Id(Long id);
//    Optional<Post> findById(Long id);

    Post getReferenceById(Long id);

    Post save(Post post);

    Page<Post> findByBoard_IdAndTitleContaining(Long boardId, String keyword, Pageable pageable);
    List<Post> findByBoard_IdAndTitleContaining(Long boardId, String keyword);


    void deleteByBoard_Id(Long id);

    void deleteById(Long id);

}
