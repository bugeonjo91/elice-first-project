package com.elice.boardproject.repository;


import com.elice.boardproject.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment,Long> {
    List<Comment> findAll();
    Comment save(Comment comment);

    Comment getReferenceById(Long id);

    void deleteById(Long id);

}
