package com.elice.boardproject.controller;


import com.elice.boardproject.entity.BoardDTO;
import com.elice.boardproject.service.CommentService;
import com.elice.boardproject.entity.PostDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@SpringBootTest
public class CommentControllerTest {

    @MockBean
    CommentService commentService;



    @Autowired
    private MockMvc mockMvc;

    List<BoardDTO> boardsDTO;
    Page<PostDTO> postsDTO;


    @Test
    public void deleteCommentTest() throws Exception {

        //given
        doNothing().when(commentService).deleteComment(anyLong());

        //when then
        mockMvc.perform(delete("/comments/{currentComment}", 1L).contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE).param("postId", "1"))
                .andExpect(redirectedUrl("/posts/1"))
                .andExpect(status().isSeeOther())
                .andDo(print());

    }








}
