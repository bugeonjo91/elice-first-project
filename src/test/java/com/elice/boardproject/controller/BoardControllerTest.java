package com.elice.boardproject.controller;


import com.elice.boardproject.entity.Board;
import com.elice.boardproject.entity.BoardDTO;
import com.elice.boardproject.etc.exception.NoSuchBoardError;
import com.elice.boardproject.mapper.BoardMapper;
import com.elice.boardproject.service.BoardService;
import com.elice.boardproject.entity.Post;
import com.elice.boardproject.entity.PostDTO;
import com.elice.boardproject.mapper.PostMapper;
import com.elice.boardproject.service.PostService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.List;

@AutoConfigureMockMvc
@SpringBootTest
public class BoardControllerTest {

    @MockBean
    BoardService boardService;

    @MockBean
    PostService postService;


    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private BoardMapper boardMapper;
    @Autowired
    private PostMapper postMapper;

    List<BoardDTO> boardsDTO;
    Page<PostDTO> postsDTO;

    List<PostDTO> postsDTOList= new ArrayList<>();


    @BeforeEach
    public void init(){
        Board board = Board.builder().name("elice").description("Start elice").build();
        Board board2 = Board.builder().name("elice3").description("Start elice3").build();
        boardsDTO = new ArrayList<>();
        boardsDTO.add(boardMapper.toDTO(board));
        boardsDTO.add(boardMapper.toDTO(board2));

        Post post = Post.builder().title("elice").content("Start elice").board(board).build();
        Post post2 = Post.builder().title("elice2").content("Start elice2").board(board2).build();

        postsDTOList.add(postMapper.toDTO(post));
        postsDTOList.add(postMapper.toDTO(post2));


        PageRequest pageRequest = PageRequest.of(0, 10);
        int start = (int) pageRequest.getOffset();
        int end = Math.min((start + pageRequest.getPageSize()), boardsDTO.size());
        postsDTO = new PageImpl<>(postsDTOList.subList(start, end), pageRequest, postsDTOList.size());

    }

    @Test
    public void getBoardsTest() throws Exception {

        //given

        given(boardService.findAllBoardsDTO()).willReturn(boardsDTO);

        //when then
        mockMvc.perform(get("/boards"))
                .andExpect(status().isOk())
                .andExpect(view().name("/board/boards"))
                .andExpect(model().attributeExists("boards"))
                .andExpect(model().attribute("boards", boardsDTO));
    }

    @Test
    public void getBoardTestWithKeyword() throws Exception {

        //given

        given(boardService.findBoardByIdDTO(1L)).willReturn(boardsDTO.get(0));
        given(postService.findPostsByBoardIdAndTitleContainingDTO(anyLong(),any(), anyInt(), anyInt())).willReturn(postsDTO);

        //when then
        mockMvc.perform(get("/boards/{id}", 1L).param("page", "2").param("size", "5").param("keyword", "1"))
                .andExpect(status().isOk())
                .andExpect(view().name("/board/board"))
                .andExpect(model().attributeExists("board"))
                .andExpect(model().attributeExists("postPage"))
                .andExpect(model().attribute("board", boardsDTO.get(0)))
                .andExpect(model().attribute("postPage", postsDTO))
                .andExpect(model().attributeExists("keyword"))
                .andDo(print());



    }

    @Test
    public void getBoardTestWithoutKeyword() throws Exception {

        //given

        given(boardService.findBoardByIdDTO(1L)).willReturn(boardsDTO.get(0));
        given(postService.findPostsByBoardIdAndTitleContainingDTO(anyLong(),any(), anyInt(), anyInt())).willReturn(postsDTO);

        //when then
        mockMvc.perform(get("/boards/{id}", 1L).param("page", "2").param("size", "5"))
                .andExpect(status().isOk())
                .andExpect(view().name("/board/board"))
                .andExpect(model().attributeExists("board"))
                .andExpect(model().attributeExists("postPage"))
                .andExpect(model().attribute("board", boardsDTO.get(0)))
                .andExpect(model().attribute("postPage", postsDTO))
                .andExpect(model().attributeDoesNotExist("keyword"))
                .andDo(print());



    }

    @Test
    public void getBoardRestTestWithoutKeyword() throws Exception {

        //given

        given(boardService.findBoardByIdDTO(1L)).willReturn(boardsDTO.get(0));
        given(postService.findPostsByBoardIdAndTitleContainingDTO(anyLong(),any())).willReturn(postsDTOList);

        //when then
        mockMvc.perform(get("/api/boards/{id}", 1L))
                .andExpect(status().isOk())
                .andDo(print());



    }



    @Test
    public void postBoardTest() throws Exception {

        //given

        given(boardService.saveBoardDTO(any(BoardDTO.class))).willReturn(boardsDTO.get(0));

        //when then
        mockMvc.perform(post("/boards/create").param("name", "1").param("description", "2"))
                .andExpect(redirectedUrl("/boards"))
                .andExpect(status().isFound())
                .andDo(print());



    }


    @Test
    public void noSuchBoardsErrorTest() throws Exception {

        //given
        given(boardService.findBoardByIdDTO(5L)).willThrow(NoSuchBoardError.class);

        //when then
        mockMvc.perform(get("/boards/{id}", 5))
                .andExpect(status().isNotFound())
                .andDo(print());



    }





}
