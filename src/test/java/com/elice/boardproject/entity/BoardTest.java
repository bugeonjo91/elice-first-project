package com.elice.boardproject.entity;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BoardTest {

    @Test
    public void BoardBuilderTest(){
        Board board = Board.builder().name("elice").description("start elice").build();


        assertEquals(board.getName(), "elice");
        assertEquals(board.getDescription(), "start elice");

    }


}
