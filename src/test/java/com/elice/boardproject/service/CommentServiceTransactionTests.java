package com.elice.boardproject.service;


import com.elice.boardproject.BoardprojectApplication;
import com.elice.boardproject.entity.Board;
import com.elice.boardproject.repository.BoardRepository;
import com.elice.boardproject.entity.Comment;
import com.elice.boardproject.entity.CommentDTO;
import com.elice.boardproject.mapper.CommentMapper;
import com.elice.boardproject.repository.CommentRepository;
import com.elice.boardproject.entity.Post;
import com.elice.boardproject.repository.PostRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@Transactional
@SpringBootTest
@ActiveProfiles("test")
@Sql("/shema.sql")
@ContextConfiguration(classes = BoardprojectApplication.class)
public class CommentServiceTransactionTests {


    @Autowired
    PostRepository postRepository;

    @Autowired
    BoardRepository boardRepository;

    @Autowired
    CommentRepository commentRepository;

    @Autowired
    CommentMapper commentMapper;

    @Mock
    PostRepository mockPostRepository;


    @Autowired
    CommentService commentService;


    @BeforeEach
    public void beforeTest(){

        //given

        Board board1 = Board.builder().name("elice1").description("start elice1").build();
        Board board2 = Board.builder().name("elice2").description("start elice2").build();

        boardRepository.save(board1);
        boardRepository.save(board2);

        Post post1 = Post.builder().title("elicePost1").content("start elicePost1").board(board1).build();
        Post post2 = Post.builder().title("elicePost2").content("start elicePost2").board(board2).build();


        Comment comment = Comment.builder().content("start eliceComment1").build();
        Comment comment2 = Comment.builder().content("start eliceComment2").build();
        commentRepository.save(comment);
        commentRepository.save(comment2);
        post1.getComments().add(comment);
        post2.getComments().add(comment2);
        postRepository.save(post1);
        postRepository.save(post2);



    }






    @Test
    public void saveCommentDTOTest(){


        //given
        CommentDTO commentDTO = CommentDTO.builder().content("test").build();
        //when
        CommentDTO foundCommentDTO = commentService.saveCommentDTO(commentDTO, 1L);

        //then
        assertEquals(3L, foundCommentDTO.getId());
        assertEquals("test", foundCommentDTO.getContent());
        assertEquals(3L, postRepository.getReferenceById(1L).getComments().get(1).getId());
        assertEquals("test", postRepository.getReferenceById(1L).getComments().get(1).getContent());

    }

    @Test
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void saveCommentDTOTransactionTest(){
        ReflectionTestUtils.setField(commentService, "postRepository", mockPostRepository);

        //given
        Board board1 = Board.builder().name("elice1").description("start elice1").build();
        Post post = Post.builder().title("elicePost1").content("start elicePost1").board(board1).build();

        CommentDTO commentDTO = CommentDTO.builder().content("test").build();
        assertEquals(2, commentService.findComments().size());

        given(mockPostRepository.getReferenceById(1L)).willReturn(post);
        given(mockPostRepository.save(any(Post.class))).willThrow(new IllegalArgumentException());


        //when
        assertThrows(RuntimeException.class, ()-> {
            commentService.saveCommentDTO(commentDTO, 1L);
        });

        //then
        assertEquals(2, commentService.findComments().size());


        ReflectionTestUtils.setField(commentService, "postRepository", postRepository);

    }


    @Test
    public void updateCommentDTOTest(){


        //given
        CommentDTO commentDTO = CommentDTO.builder().id(1L).content("test").build();
        //when
        CommentDTO foundCommentDTO = commentService.updateCommentDTO(commentDTO);

        //then
        assertEquals(1L, foundCommentDTO.getId());
        assertEquals("test", foundCommentDTO.getContent());
        assertEquals(1L, postRepository.getReferenceById(1L).getComments().get(0).getId());
        assertEquals("test", postRepository.getReferenceById(1L).getComments().get(0).getContent());

    }



}
