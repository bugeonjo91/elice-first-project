package com.elice.boardproject.repository;


import com.elice.boardproject.entity.Board;
import com.elice.boardproject.entity.Comment;
import com.elice.boardproject.entity.Post;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;

import javax.sql.DataSource;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DataJpaTest
@Sql("/shema.sql")
public class CommentRepositoryTests {

    PostRepository postRepository;

    BoardRepository boardRepository;

    @Autowired
    CommentRepository commentRepository;

    @Autowired
    public CommentRepositoryTests(DataSource dataSource, PostRepository postRepository) {

        this.boardRepository = new JdbcTemplateBoardRepository(dataSource, postRepository);
        this.postRepository = postRepository;
    }




    @BeforeEach
    public void beforeTest(){

        Board board1 = Board.builder().name("elice1").description("start elice1").build();
        Board board2 = Board.builder().name("elice2").description("start elice2").build();

        boardRepository.save(board1);
        boardRepository.save(board2);

        Post post1 = Post.builder().title("elicePost1").content("start elicePost1").board(board1).build();
        Post post2 = Post.builder().title("elicePost2").content("start elicePost2").board(board2).build();


        Comment comment = Comment.builder().content("start eliceComment1").build();
        Comment comment2 = Comment.builder().content("start eliceComment2").build();
        commentRepository.save(comment);
        commentRepository.save(comment2);
        post1.getComments().add(comment);
        post2.getComments().add(comment2);
        postRepository.save(post1);
        postRepository.save(post2);
    }


    @Test
    public void findCommentTest(){
        Comment comment1 = commentRepository.getReferenceById(1L);
        Comment comment2 = commentRepository.getReferenceById(2L);

        assertEquals(comment1.getContent(),"start eliceComment1");
        assertEquals(comment2.getContent(),"start eliceComment2");
        assertThrows(EntityNotFoundException.class, ()-> System.out.println(postRepository.getReferenceById(3L)));


        List<Comment> comments = commentRepository.findAll();

        assertEquals(comment1.getId(), comments.get(0).getId());
        assertEquals(comment1.getContent(), comments.get(0).getContent());
        assertEquals(comment2.getId(), comments.get(1).getId());
        assertEquals(comment2.getContent(), comments.get(1).getContent());
    }

    @Test
    public void saveCommentTest(){

        Post post = postRepository.getReferenceById(1L);
        Comment comment1 = Comment.builder().content("start eliceComment3").build();
        Comment comment2 = Comment.builder().content("start eliceComment4").build();

        Comment createdComment1 = commentRepository.save(comment1);
        Comment createdComment2 = commentRepository.save(comment2);

        post.getComments().add(comment1);
        post.getComments().add(comment2);
        postRepository.save(post);



        assertEquals(createdComment1.getId(), 3L);
        assertEquals(createdComment1.getContent(), "start eliceComment3");

        assertEquals(createdComment2.getId(), 4L);
        assertEquals(createdComment2.getContent(), "start eliceComment4");

    }



    @Test
    public void updateCommentTest(){

        Post post = postRepository.getReferenceById(1L);
        Comment comment = Comment.builder().content("start bob").id(1L).build();

        commentRepository.save(comment);
        List<Comment> comments = commentRepository.findAll();


        assertEquals(1L, comments.get(0).getId());
        assertEquals("start bob", comments.get(0).getContent());
        assertEquals(2L, comments.get(1).getId());
        assertEquals("start eliceComment2", comments.get(1).getContent());


    }


    @Test
    public void deleteCommentTest(){


        commentRepository.deleteById(1L);
        List<Post> posts = postRepository.findAll();
        List<Board> boards = boardRepository.findAll();
        List<Comment> comments = commentRepository.findAll();


        assertEquals(1, comments.size());
        assertEquals(2L, comments.get(0).getId());
        assertEquals("start eliceComment2", comments.get(0).getContent());


        assertEquals(2, boards.size());
        assertEquals(2, posts.size());

        //TO DO: 이거 에러 발생 시켜야 하는데 잘 모르겠음.

//        assertThrows(EmptyResultDataAccessException.class, ()-> postRepository.deleteById(3L));


    }



}
