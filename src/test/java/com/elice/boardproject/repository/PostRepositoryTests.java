package com.elice.boardproject.repository;


import com.elice.boardproject.entity.Board;
import com.elice.boardproject.entity.Comment;
import com.elice.boardproject.entity.Post;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;

import javax.sql.DataSource;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
@DataJpaTest
@Sql("/shema.sql")
public class PostRepositoryTests {

    PostRepository postRepository;

    BoardRepository boardRepository;

    @Autowired
    CommentRepository commentRepository;

    @Autowired
    public PostRepositoryTests(DataSource dataSource, PostRepository postRepository) {

        this.boardRepository = new JdbcTemplateBoardRepository(dataSource, postRepository);
        this.postRepository = postRepository;
    }




    @BeforeEach
    public void beforeTest(){

        Board board1 = Board.builder().name("elice1").description("start elice1").build();
        Board board2 = Board.builder().name("elice2").description("start elice2").build();

        boardRepository.save(board1);
        boardRepository.save(board2);

        Post post1 = Post.builder().title("elicePost1").content("start elicePost1").board(board1).build();
        Post post2 = Post.builder().title("elicePost2").content("start elicePost2").board(board2).build();

        Comment comment = Comment.builder().content("start eliceComment1").build();
        Comment comment2 = Comment.builder().content("start eliceComment2").build();
        commentRepository.save(comment);
        commentRepository.save(comment2);
        post1.getComments().add(comment);
        post2.getComments().add(comment2);
        postRepository.save(post1);
        postRepository.save(post2);
    }


    @Test
    public void savePostTest(){

        Board board = boardRepository.findById(1L);
        Post post1 = Post.builder().title("elicePost1").content("start elicePost1").board(board).build();
        Post post2 = Post.builder().title("elicePost2").content("start elicePost2").board(board).build();
        Post createdPost1  = postRepository.save(post1);
        Post createdPost2  = postRepository.save(post2);



        assertEquals(createdPost1.getId(), 3L);
        assertEquals(createdPost1.getTitle(), "elicePost1");
        assertEquals(createdPost1.getContent(), "start elicePost1");

        assertEquals(createdPost2.getId(), 4L);
        assertEquals(createdPost2.getTitle(), "elicePost2");
        assertEquals(createdPost2.getContent(), "start elicePost2");

    }





    @Test
    public void findPostTest(){
        Post post1 = postRepository.getReferenceById(1L);
        Post post2 = postRepository.getReferenceById(2L);

        assertEquals(post1.getTitle(),"elicePost1");
        assertEquals(post2.getTitle(),"elicePost2");
        assertEquals(post1.getContent(),"start elicePost1");
        assertEquals(post2.getContent(),"start elicePost2");
        assertThrows(EntityNotFoundException.class, ()-> System.out.println(postRepository.getReferenceById(3L)));


        List<Post> posts = postRepository.findAll();

        assertEquals(post1.getId(), posts.get(0).getId());
        assertEquals(post1.getTitle(), posts.get(0).getTitle());
        assertEquals(post1.getContent(), posts.get(0).getContent());
        assertEquals(post2.getId(), posts.get(1).getId());
        assertEquals(post2.getTitle(), posts.get(1).getTitle());
        assertEquals(post2.getContent(), posts.get(1).getContent());
    }


    @Test
    public void updatePostTest(){

        Board board = boardRepository.findById(1L);
        Post post = Post.builder().title("bob").content("start bob").id(1L).board(board).build();
        postRepository.save(post);
        List<Post> posts = postRepository.findAll();


        assertEquals(1L, posts.get(0).getId());
        assertEquals("bob", posts.get(0).getTitle());
        assertEquals("start bob", posts.get(0).getContent());
        assertEquals(2L, posts.get(1).getId());
        assertEquals("elicePost2", posts.get(1).getTitle());
        assertEquals("start elicePost2", posts.get(1).getContent());


    }


    @Test
    public void deletePostTest(){

        postRepository.getReferenceById(1L).getBoard().getPosts().clear();
        postRepository.deleteById(1L);


        List<Post> posts = postRepository.findAll();
        List<Board> boards = boardRepository.findAll();


        assertEquals(1, posts.size());
        assertEquals(2L, posts.get(0).getId());
        assertEquals("elicePost2", posts.get(0).getTitle());
        assertEquals("start elicePost2", posts.get(0).getContent());


        assertEquals(2, boards.size());

        //TO DO: 이거 에러 발생 시켜야 하는데 잘 모르겠음.

//        assertThrows(EmptyResultDataAccessException.class, ()-> postRepository.deleteById(3L));


    }

    @Test
    public void deletePostWithCommentTest(){


        postRepository.deleteById(1L);

        List<Board> boards = boardRepository.findAll();
        List<Post> posts = postRepository.findAll();
        List<Comment> comments = commentRepository.findAll();

        assertEquals(2, boards.size());

        assertEquals(1, posts.size());
        assertEquals(2L, posts.get(0).getId());
        assertEquals("elicePost2", posts.get(0).getTitle());
        assertEquals("start elicePost2", posts.get(0).getContent());

        //TO DO: 이거 에러 발생 시켜야 하는데 잘 모르겠음.

//        assertThrows(EmptyResultDataAccessException.class, ()-> postRepository.deleteById(3L));



        assertEquals(1, comments.size());
        assertEquals(2L, comments.get(0).getId());
        assertEquals("start eliceComment2", comments.get(0).getContent());

    }




}
