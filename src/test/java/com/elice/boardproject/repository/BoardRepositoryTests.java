package com.elice.boardproject.repository;

import com.elice.boardproject.BoardprojectApplication;
import com.elice.boardproject.entity.Board;
import com.elice.boardproject.entity.Post;
import com.elice.boardproject.etc.exception.NoSuchBoardError;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


@Transactional
@SpringBootTest
@ActiveProfiles("test")
@Sql("/shema.sql")
@ContextConfiguration(classes = BoardprojectApplication.class)
class BoardRepositoryTests {


	@Autowired
	BoardRepository boardRepository;

	@Autowired
	PostRepository postRepository;


//	@Transactional
	@BeforeEach
	public void beforeTest(){

		Board board = Board.builder().name("elice").description("start elice").build();
		Board board2 = Board.builder().name("elice2").description("start elice2").build();
		boardRepository.save(board);
		boardRepository.save(board2);
	}


	@Test
	public void saveBoardTest(){

		Board board1 = Board.builder().name("elice").description("start elice").build();
		Board boardCreated1 = boardRepository.save(board1);
		Board board2 = Board.builder().name("elice2").description("start elice2").build();
		Board boardCreated2 = boardRepository.save(board2);


		assertEquals(boardCreated1.getId(), 3L);
		assertEquals(boardCreated1.getName(), "elice");
		assertEquals(boardCreated1.getDescription(), "start elice");

		assertEquals(boardCreated2.getId(), 4L);
		assertEquals(boardCreated2.getName(), "elice2");
		assertEquals(boardCreated2.getDescription(), "start elice2");

	}


	@Test
	public void findBoardTest(){
		Board board1 = boardRepository.findById(1L);
		Board board2 = boardRepository.findById(2L);

		assertEquals(board1.getName(),"elice");
		assertEquals(board2.getName(),"elice2");
		assertEquals(board1.getDescription(),"start elice");
		assertEquals(board2.getDescription(),"start elice2");
		assertThrows(NoSuchBoardError.class, ()-> boardRepository.findById(3L));


		List<Board> boards = boardRepository.findAll();

		assertEquals(board1.getId(), boards.get(0).getId());
		assertEquals(board1.getName(), boards.get(0).getName());
		assertEquals(board1.getDescription(), boards.get(0).getDescription());
		assertEquals(board2.getId(), boards.get(1).getId());
		assertEquals(board2.getName(), boards.get(1).getName());
		assertEquals(board2.getDescription(), boards.get(1).getDescription());
	}

	@Test
	public void updateBoardTest(){
		Board board = Board.builder().id(1L).name("bob").description("start bob").build();
		int count = boardRepository.update(board);
		List<Board> boards = boardRepository.findAll();


		assertEquals(1, count);
		assertEquals(1L, boards.get(0).getId());
		assertEquals("bob", boards.get(0).getName());
		assertEquals("start bob", boards.get(0).getDescription());
		assertEquals(2L, boards.get(1).getId());
		assertEquals("elice2", boards.get(1).getName());
		assertEquals("start elice2", boards.get(1).getDescription());


		Board board2 = Board.builder().id(3L).name("bob").description("start bob").build();
		assertThrows(NoSuchBoardError.class, ()-> boardRepository.update(board2));

	}


	@Test
	public void deleteBoardTest(){

		boardRepository.delete(1L);
		List<Board> boards = boardRepository.findAll();


		assertEquals(1, boards.size());
		assertEquals(2L, boards.get(0).getId());
		assertEquals("elice2", boards.get(0).getName());
		assertEquals("start elice2", boards.get(0).getDescription());

		assertThrows(NoSuchBoardError.class, ()-> boardRepository.delete(3L));


	}

	@Test
	public void deleteBoardWithPostTest(){

		List<Board> boards = boardRepository.findAll();

		Post post1 = Post.builder().title("elicePost1").content("start elicePost1").board(boards.get(0)).build();
		Post post2 = Post.builder().title("elicePost2").content("start elicePost2").board(boards.get(1)).build();
		postRepository.save(post1);
		postRepository.save(post2);

		postRepository.deleteByBoard_Id(1L);
		postRepository.flush();
		boardRepository.delete(1L);
		boards = boardRepository.findAll();
		List<Post> posts = postRepository.findAll();

		assertEquals(1, boards.size());
		assertEquals(2L, boards.get(0).getId());
		assertEquals("elice2", boards.get(0).getName());
		assertEquals("start elice2", boards.get(0).getDescription());
		assertThrows(NoSuchBoardError.class, ()-> boardRepository.delete(3L));

		assertEquals(1, posts.size());
		assertEquals(2L, posts.get(0).getId());
		assertEquals("elicePost2", posts.get(0).getTitle());
		assertEquals("start elicePost2", posts.get(0).getContent());
	}


}
